import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';
import TaskContext from '../../context/TaskContext';
import TaskActionContext from '../../context/TaskActionContext';
import TaskForm from './TaskForm';
import { useTaskUpdate } from './form';

export default function TaskUpdate({ handleClose }) {
  const { subject, id } = useContext(TaskContext);
  const { onUpdate } = useContext(TaskActionContext);
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
  } = useForm({ defaultValues: { subject } });
  const updateSubject = useTaskUpdate({
    id,
    onChange: () => {
      onUpdate();
      handleClose ? handleClose() : undefined;
    },
    setError,
  });
  return (
    <TaskForm
      errors={errors}
      onSubmit={handleSubmit((data) => {
        updateSubject(data.subject);
      })}
      register={register}
    />
  );
}
