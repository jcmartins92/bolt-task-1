import React, { useContext, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ProjectContext from '../context/ProjectContext';
import ProjectActionContext from '../context/ProjectActionContext';
import axios from 'axios';
import { useRouter } from 'next/router';
import { responseToastError } from '../Login/helpers';

export default function ProjectDelete() {
  const { id } = useContext(ProjectContext);
  const { onDelete } = useContext(ProjectActionContext);
  const [open, setOpen] = useState(false);
  const router = useRouter();
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleDelete = () => {
    axios
      .delete(`/api/projects/${id}`)
      .then(function (response) {
        if (response.status >= 200) {
          if (id) {
            onDelete();
          }
          router.push('/projects');
          handleClose();
        }
      })
      .catch(function (error) {
        responseToastError(error);
      });
    setOpen(false);
  };

  return (
    <>
      <Button variant="outlined" color="secondary" onClick={handleClickOpen}>
        Delete
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby={'project-remove'}
        aria-describedby={'project-remove-description'}
      >
        <DialogTitle id="project-remove">Remove the project</DialogTitle>
        <DialogContent>
          <DialogContentText id="project-remove-description">
            This action is irreversible <br />
            It will remove all the tasks of this project
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDelete} color="secondary">
            Remove
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
