import React from 'react';
import { NextPage } from 'next';
import LayoutPictureForm from '../components/Layout/LayoutPictureForm';
import FormRegister from '../components/Login/FormRegister';

const Register: NextPage = () => {
  return (
    <LayoutPictureForm>
      <FormRegister />
    </LayoutPictureForm>
  );
};
export default Register;
