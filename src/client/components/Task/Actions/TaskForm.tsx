import React from 'react';
import TextField from '@material-ui/core/TextField';
import { get } from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import { Column, Item, Row } from '@mui-treasury/components/flex';
import IconButton from '@material-ui/core/IconButton';
import AddBox from '@material-ui/icons/AddBox';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    marginBottom: theme.spacing(1),
  },
}));

export default function TaskForm({ onSubmit, register, errors }) {
  const classes = useStyles();
  return (
    <form onSubmit={onSubmit} className={classes.form} noValidate>
      <Column>
        <Row gap={1}>
          <Item stretched={true}>
            <TextField
              variant={'outlined'}
              margin={'normal'}
              required
              fullWidth={true}
              label={'Subject'}
              id={'subject'}
              autoFocus={true}
              inputProps={register('subject')}
              helperText={get(errors, 'subject', null)}
              error={Boolean(get(errors, 'subject'))}
            />
          </Item>
          <Item position={'middle'}>
            <IconButton type={'submit'} color={'primary'} size={'medium'}>
              <AddBox />
            </IconButton>
          </Item>
        </Row>
      </Column>
    </form>
  );
}
