# ⚡ BoltTask ⚡

This projects it's an exercise for interview at bolttech, it runs on docker, and use NEXTJS to frontend, and NestJs to Backend,
base on starter projects on github, its not intended to be used to other scenario than what is proposed, the interview.
---

### Branches 
1. merge  at 2021/05/10

---

Must have Features

1. [x] User Registration
1. [x] User Authentication (login/logout)
1. [x] Visualize, add, edit and remove user projects
1. [x] Visualize, add, edit and remove tasks associated with the projects

---
## Requirements:
1. [x] One user may have several projects
1. [x] One user can access his projects only
1. [x] Each project may include multiple tasks
1. [x] Each task must have a description, creation date and finish date
1. [] The user needs to have a simple option to mark the tasks as completed when accessing the task list
1. [x] Each task should have its termination date visible as a tooltip, if available, and some visual way of identifying
   its status
7. [x] A task that was defined as finished should not be edited nor removed
8. [x] When a task or Project is added our deleted, the page should not fully refresh, so that users have a good
   experience

---

## Non funcional requirements

1. [x] The application should be written in <s>Javascript</s> i have done in  <u>Typescrip</u>.
2. [x] The application backend should be written in Node.js or GoLang.
3. [] The authentication and registration layers should be coded and not based on pre-existing modules (such as
   Passport)| it uses nestjs/jtk .
4. [x] For the frontend, javascript frameworks can be used (Angular, React, Polymer or others).
5. [x] Components should be used to promote increased code reusage (react or angular components,
   webcomponents or other alternatives)

---

## Extras:

1. [x] Build tools
   (e.g. grunt or gulp) <u>Docker</u>
1. [x] Unit test jest
1. [x] e2e tests 
---
#### non-important features 

1. [x] Search by project, ready in **dev-0.0.2**
1. [x] Search by task, ready in **dev-0.0.2**
1. [x] Redirect not login ready in **dev-0.0.2**


---

## Stack

- Docker setup
- Typescript, ESLint
- CI via GitHub Actions
- Jest

## Usage
```sh
cp .env.example .env
docker-compose up
docker-compose exec web yarn lint
docker-compose exec db psql -U postgres -c 'create database test;'
docker-compose exec web yarn test
docker-compose exec web yarn test:e2e
docker-compose exec web yarn build
```


Page
- http://localhost:3333/

Register
- http://localhost:3333/register

Login
- http://localhost:3333/login

Projects
- http://localhost:3333/projects

REST endpoint via Nest
- http://localhost:3333/hello_nest

JWT-protected REST endpoint via Nest
- http://localhost:3333/hello_private


GraphQL playground (`query WhoAmI` is JWT-protected)
- http://localhost:3333/graphql




### Useful commands

Nest CLI:
```
docker-compose exec web yarn nest -- --help
```

TypeORM CLI:
```
docker-compose exec web yarn typeorm -- --help
```


uninstall:
```
docker-compose down
```


##### Copyright
Carlos Martins

 
