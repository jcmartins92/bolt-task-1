import {
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Body } from '@nestjs/common/decorators/http/route-params.decorator';
import { ProjectsService } from '../projects.service';
import { CreateProjectDto, UpdateProjectDto } from '../dto/create-project.dto';
import { Project } from '../project.entity';
import { JwtAuthGuard } from '../../auth/jwt/jwt-auth.guard';

@Controller('/api/projects')
export class ProjectsController {
  constructor(private projectsService: ProjectsService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Request() req, @Body() project: CreateProjectDto): Promise<Project> {
    project.user = req.user;
    return this.projectsService.create(project);
  }

  @UseGuards(JwtAuthGuard)
  @Put('/:id')
  update(
    @Param('id') id: string,
    @Body() data: UpdateProjectDto,
  ): Promise<Project> {
    return this.projectsService.update(id, data);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  delete(@Param('id') id: string): Promise<Project> {
    return this.projectsService.delete(id);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(): Promise<Project[]> {
    return this.projectsService.findAll();
  }
}
