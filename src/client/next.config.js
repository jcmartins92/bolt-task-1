module.exports = {
  typescript: {
    ignoreBuildErrors: true,
  },
  distDir: '../../.next',
};
