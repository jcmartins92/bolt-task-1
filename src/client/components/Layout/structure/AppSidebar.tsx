import React from 'react';
import List from '@material-ui/core/List';
import WhiteIconItem from './WhiteIconItem';
import TextLogo from './TextLogo';
import DnsRounded from '@material-ui/icons/DnsRounded';

const list = [
  {
    href: '/projects',
    primaryText: 'Projects',
    icon: <DnsRounded />,
    active: true,
  },
];

const RoundIconSidebar = () => {
  return (
    <List>
      <TextLogo mt={2} mb={4}>
        ⚡
      </TextLogo>
      {list.map((item) => (
        <WhiteIconItem href={item.href} key={item.primaryText} {...item} />
      ))}
    </List>
  );
};

export default RoundIconSidebar;
