import React, { useContext, useState } from 'react';
import TaskContext from '../context/TaskContext';
import TaskItemUpdate from './TaskItemUpdate';
import TaskItemView from './TaskItemView';

export default function TaskItem({}) {
  const [open, setOpen] = useState(false);
  const { complete } = useContext(TaskContext);
  const toggleOpen = () => {
    if (!complete) {
      setOpen(!open);
    }
  };
  if (open) {
    return <TaskItemUpdate toggleOpen={toggleOpen} />;
  }
  return <TaskItemView toggleOpen={toggleOpen} />;
}
