import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import ProjectDialog from './ProjectDialog';
import ProjectForm from './ProjectForm';

type ProjectActionButtonType = {
  label: string;
  size?: 'small' | 'medium' | 'large';
};
export default function ProjectActionButton({
  size,
  label,
}: ProjectActionButtonType) {
  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Button
        size={size}
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
      >
        {label}
      </Button>
      <ProjectDialog handleClose={handleClose} open={open}>
        <ProjectForm handleClose={handleClose} />
      </ProjectDialog>
    </>
  );
}
