import { User } from '../../users/user.entity';

export class CreateProjectDto {
  alias: string;
  user: User;
}

export class UpdateProjectDto {
  alias: string;
}
