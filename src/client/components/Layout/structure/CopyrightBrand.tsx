import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import React from 'react';

export default function CopyrightBrand() {
  return (
    <Typography variant={'body2'} color={'textSecondary'} align={'center'}>
      {'Copyright © '}
      <Link target={'_blank'} color={'inherit'} href={'https://agem.io/'}>
        Carlos Martins
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}
