import React, { useContext } from 'react';
import Avatar from '@material-ui/core/Avatar';
import ProfileContext from './ProfileContext';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { useDynamicAvatarStyles } from '@mui-treasury/styles/avatar/dynamic';
import Link from '@material-ui/core/Link';

function initials(username) {
  const name = `${username || ''}`;
  let letters = name[0];
  const allWords = name.split(' ');
  if (allWords.length > 1) {
    const second = allWords[allWords.length - 1][0];
    letters += `${second}`;
  }
  return letters ? letters.toUpperCase() : '-';
}

export default function ProfileAvatar() {
  const { username } = useContext(ProfileContext);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const avatarStyles = useDynamicAvatarStyles({ radius: 12, size: 40 });

  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);

  return (
    <>
      <IconButton onClick={handleClick}>
        <Avatar classes={avatarStyles}>{initials(username)}</Avatar>
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        <MenuItem component={Link} href={'/auth/logout'}>
          Logout
        </MenuItem>
      </Menu>
    </>
  );
}
