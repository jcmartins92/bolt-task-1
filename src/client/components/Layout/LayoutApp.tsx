import React from 'react';
import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Layout, {
  getContent,
  getDrawerSidebar,
  getFooter,
  getHeader,
  getSidebarContent,
  getSidebarTrigger,
  Root,
} from '@mui-treasury/layout';
import Container from '@material-ui/core/Container';
import CopyrightBrand from './structure/CopyrightBrand';
import AppSidebar from './structure/AppSidebar';
import HeaderContent from './structure/HeaderContent';
import { ToastContainer } from 'react-toastify';

const Header = getHeader(styled);
const DrawerSidebar = getDrawerSidebar(styled);
const SidebarTrigger = getSidebarTrigger(styled);
const SidebarContent = getSidebarContent(styled);
const Content = getContent(styled);
const Footer = getFooter(styled);

const useStyles = makeStyles(() => ({
  header: {
    backgroundColor: '#fff',
  },
  sidebar: {
    backgroundColor: '#36338E',
  },
  container: {
    marginTop: '100px',
    minHeight: '90vh',
  },
}));
const scheme = Layout();
scheme.configureHeader((builder) => {
  builder.registerConfig('xs', {
    position: 'sticky',
    initialHeight: 60,
    clipped: false,
  });
});
scheme.configureEdgeSidebar((builder) => {
  builder
    .create('primarySidebar', { anchor: 'left' })
    .registerTemporaryConfig('xs', {
      width: 84,
    });
});

const LayoutApp = ({ children }) => {
  const styles = useStyles();
  return (
    <Root
      scheme={scheme}
    >
      <DrawerSidebar
        sidebarId={'primarySidebar'}
        PaperProps={{ className: styles.sidebar }}
      >
        <SidebarContent>
          <AppSidebar />
        </SidebarContent>
      </DrawerSidebar>
      <Header className={styles.header}>
        <Toolbar>
          <SidebarTrigger sidebarId={'primarySidebar'} />
          <HeaderContent />
        </Toolbar>
      </Header>
      <Content>
        <Container classes={{ root: styles.container }}>
          <br />
          {children}
        </Container>
      </Content>
      <Footer>
        <CopyrightBrand />
      </Footer>
      <CssBaseline />
      <ToastContainer />
    </Root>
  );
};

export default LayoutApp;
