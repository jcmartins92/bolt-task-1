import React, { Fragment, useContext, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { useTypedQuery } from '../app/apollo-client';
import Typography from '@material-ui/core/Typography';
import AppSearchBar from './Structure/AppSearchBar';
import Divider from '@material-ui/core/Divider';

import ProjectHeader from './Structure/ProjectHeader';
import ProjectContext from './context/ProjectContext';
import TaskActionContext from './context/TaskActionContext';
import TaskContext from './context/TaskContext';
import TaskItem from './Task/TaskItem';
import TaskCreate from './Task/Actions/TaskCreate';

const useStyles = makeStyles(() => ({
  paper: {
    maxWidth: 936,
    margin: 'auto',
    overflow: 'hidden',
  },
  contentWrapper: {
    margin: '16px',
  },
}));
export default function ProjectView() {
  const classes = useStyles();
  const { id } = useContext(ProjectContext);
  const [search, setSearch] = useState('');
  const { loading, error, data, refetch } = useTypedQuery({
    searchTasksByProject: [
      { projectId: id, subject: search },
      {
        id: true,
        complete: true,
        subject: true,
        updated_at: true,
        complete_date: true,
        created_at: true,
      },
    ],
  });
  const handleSearch = (value) => {
    setSearch(value);
  };
  const hasData = Boolean(data && data.searchTasksByProject.length);

  return (
    <TaskActionContext.Provider
      value={{
        onCreate: refetch,
        onUpdate: refetch,
        onDelete: refetch,
      }}
    >
      <Paper className={classes.paper}>
        <ProjectHeader />
        <Divider />
        <AppSearchBar
          placeholder={'Search by task subject'}
          onSearch={handleSearch}
          search={search}
          loading={loading}
          onRefresh={refetch}
        />
        <div className={classes.contentWrapper}>
          {error && <Typography>{JSON.stringify(error)}</Typography>}
          <TaskCreate />
          <Divider />
          {hasData &&
            data.searchTasksByProject.map((task) => (
              <Fragment key={task.id}>
                <TaskContext.Provider value={task}>
                  <TaskItem />
                </TaskContext.Provider>
                <Divider />
              </Fragment>
            ))}
        </div>
      </Paper>
    </TaskActionContext.Provider>
  );
}
