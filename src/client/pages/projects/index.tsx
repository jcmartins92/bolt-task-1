import React from 'react';
import { NextPage } from 'next';
import { Request } from 'express';
import ProfileContext from '../../components/profile/ProfileContext';
import LayoutApp from '../../components/Layout/LayoutApp';
import ProjectsContent from '../../components/ProjectsList';
import client from '../../app/apollo-client';
import { ApolloProvider } from '@apollo/client/react';

const Projects: NextPage<{ user: Request['user'] }> = ({ user }) => {
  return (
    <ProfileContext.Provider value={user}>
      <ApolloProvider client={client}>
        <LayoutApp>
          <ProjectsContent />
        </LayoutApp>
      </ApolloProvider>
    </ProfileContext.Provider>
  );
};

export async function getServerSideProps({ req }) {
  return {
    props: { user: (req as Request).user || '' },
  };
}

export default Projects;
