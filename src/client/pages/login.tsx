import React from 'react';
import { NextPage } from 'next';
import LayoutPictureForm from '../components/Layout/LayoutPictureForm';
import FormLogin from '../components/Login/FormLogin';
const Login: NextPage = () => {
  return (
    <LayoutPictureForm>
      <FormLogin />
    </LayoutPictureForm>
  );
};

export default Login;
