import React from 'react';
import PropTypes from 'prop-types';
import cx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Link from '@material-ui/core/Link';
import { useRouter } from 'next/router';

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 16,
    paddingBottom: 16,
    '&:hover': {
      background: 'none',
      '& $icon': {
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
      },
    },
  },
  icon: {
    minWidth: 0,
    borderRadius: 12,
    padding: 8,
    '& svg': {
      color: '#fff',
      fontSize: 32,
    },
  },
  activeIcon: {
    border: '2px solid rgba(255,255,255,0.7)',
  },
}));

const WhiteIconItem = ({ active, href, icon }) => {
  const router = useRouter();
  const styles = useStyles();
  return (
    <ListItem
      onClick={() => router.push(href)}
      component={Link}
      className={styles.root}
      button
    >
      <ListItemIcon className={cx(styles.icon, active && styles.activeIcon)}>
        {icon}
      </ListItemIcon>
    </ListItem>
  );
};

WhiteIconItem.propTypes = {
  icon: PropTypes.node,
};
WhiteIconItem.defaultProps = {
  icon: null,
};

export default WhiteIconItem;
