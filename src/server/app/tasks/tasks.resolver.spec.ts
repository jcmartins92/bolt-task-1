import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getConnection } from 'typeorm';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../users/users.service';
import { TasksModule } from './tasks.module';
import { TasksResolver } from './tasks.resolver';
import { TasksService } from './tasks.service';
import { projectsFactory, tasksFactory, usersFactory } from 'test/factories';
import { ProjectsService } from '../projects/projects.service';
import { ProjectsModule } from '../projects/projects.module';
import { TaskCompleteException } from './exeception/TaskCompleteException';

describe('TasksResolver', () => {
  let resolver: TasksResolver;
  let tasksService: TasksService;
  let usersService: UsersService;
  let projectsService: ProjectsService;
  let moduleRef: TestingModule;

  beforeEach(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          url: process.env.DATABASE_URL,
        }),
        ProjectsModule,
        UsersModule,
        TasksModule,
      ],
    }).compile();

    resolver = moduleRef.get<TasksResolver>(TasksResolver);
    tasksService = moduleRef.get<TasksService>(TasksService);
    projectsService = moduleRef.get<ProjectsService>(ProjectsService);
    usersService = moduleRef.get<UsersService>(UsersService);
    await getConnection().synchronize(true);
  });

  afterEach(async () => {
    await moduleRef.close();
  });

  describe('TasksResolver', () => {
    async function createProjectUser() {
      const user = await usersService.create(usersFactory.build());
      const project = await projectsService.create(
        projectsFactory.build(
          {},
          {
            associations: {
              user: user,
            },
          },
        ),
      );
      return { user, project };
    }

    it('returns tasks of user', async () => {
      const { user, project } = await createProjectUser();
      const task = await tasksService.create(
        tasksFactory.build(
          {},
          { associations: { user: user, project: project } },
        ),
      );
      const result = await resolver.tasks(user);
      expect([task]).toMatchObject(result);
    });
    it('returns tasks project', async () => {
      const { user, project } = await createProjectUser();
      const task = await tasksService.create(
        tasksFactory.build(
          {},
          { associations: { user: user, project: project } },
        ),
      );
      const result = await resolver.project(task);
      expect(result.id).toBe(project.id);
    });

    it('searchTasksByProject search like% and user', async () => {
      const { user, project } = await createProjectUser();
      const anotherUser = await usersService.create(usersFactory.build());
      await resolver.createTask(user, project.id, 'hello Bolt ', false);
      let result = await resolver.searchTasksByProject(
        user,
        project.id,
        'hello',
      );
      expect(result.length).toEqual(1);
      result = await resolver.searchTasksByProject(
        anotherUser,
        project.id,
        'hello',
      );
      expect(result.length).toEqual(0);
      result = await resolver.searchTasksByProject(user, project.id, 'Bol');
      expect(result.length).toEqual(0);
    });

    it('does not return tasks of another user', async () => {
      const anotherUser = await usersService.create(usersFactory.build());
      const { user, project } = await createProjectUser();
      await tasksService.create(
        tasksFactory.build(
          {},
          { associations: { user: anotherUser, project: project } },
        ),
      );
      const result = await resolver.tasks(user);
      expect(result).toEqual([]);
    });

    it('returns the task', async () => {
      const { user, project } = await createProjectUser();
      const subject = tasksFactory.build().subject;
      const result = await resolver.createTask(
        user,
        project.id,
        subject,
        false,
      );
      expect(result).toMatchObject({ subject: subject });
    });
    it('creates an task', async () => {
      const { user, project } = await createProjectUser();
      const subject = tasksFactory.build().subject;
      await resolver.createTask(user, project.id, subject, false);
      const tasksCount = (await tasksService.findAll({ where: { user: user } }))
        .length;
      expect(tasksCount).toEqual(1);
    });

    it('toggleTask need to complete task and set the date ', async () => {
      const { user, project } = await createProjectUser();
      const subject = tasksFactory.build().subject;
      const task = await resolver.createTask(user, project.id, subject, false);
      expect(task.complete).toEqual(false);
      expect(task.complete_date).toBe(null);
      let toggleTask = await resolver.toggleTask(task.id);
      expect(toggleTask.complete).toEqual(true);
      expect(toggleTask.complete_date).toBeDefined();
    });

    it('should throw exception when toggleTask and complete creation is false  ', async () => {
      const { user, project } = await createProjectUser();
      const subject = tasksFactory.build().subject;
      let newTask = await resolver.createTask(user, project.id, subject, false);
      await resolver.toggleTask(newTask.id);
      try {
        await resolver.toggleTask(newTask.id);
      } catch (e) {
        expect(e).toBeInstanceOf(TaskCompleteException);
      }
    });
    it('should throw exception when toggleTask is complete and creation is true  ', async () => {
      const { user, project } = await createProjectUser();
      const subject = tasksFactory.build().subject;
      let newTask = await resolver.createTask(user, project.id, subject, true);
      try {
        await resolver.toggleTask(newTask.id);
      } catch (e) {
        expect(e).toBeInstanceOf(TaskCompleteException);
      }
    });

    it('updateSubject', async () => {
      const { user, project } = await createProjectUser();
      const subject = tasksFactory.build().subject;
      const task = await resolver.createTask(user, project.id, subject, false);
      expect(task.subject).toEqual(subject);
      const newSubject = tasksFactory.build().subject;
      const taskUpdated = await resolver.updateSubject(task.id, newSubject);
      expect(taskUpdated.subject).toBe(newSubject);
    });

    it('removeTask', async () => {
      const { user, project } = await createProjectUser();
      const subject = tasksFactory.build().subject;
      const task = await resolver.createTask(user, project.id, subject, false);
      expect(task).toBeDefined();
      const taskRemoved = await resolver.removeTask(task.id);
      expect(taskRemoved.subject).toBe(task.subject);
      const findTask = await tasksService.findOne({ where: { id: task.id } });
      expect(findTask).toBeUndefined();
    });

    it('does not create the same task twice', async () => {
      const { user, project } = await createProjectUser();
      const subject = tasksFactory.build().subject;
      await resolver.createTask(user, project.id, subject, false);
      await resolver.createTask(user, project.id, subject, false);
      const taskCount = (await tasksService.findAll({ where: { user: user } }))
        .length;
      expect(taskCount).toEqual(1);
    });
  });
});
