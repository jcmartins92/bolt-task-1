import React from 'react';
import { NextPage } from 'next';
import { Request } from 'express';
import ProfileContext from '../../components/profile/ProfileContext';
import LayoutApp from '../../components/Layout/LayoutApp';
import client, { typedQuery } from '../../app/apollo-client';
import { ApolloProvider } from '@apollo/client/react';
import ProjectView from '../../components/ProjectsView';
import ProjectActionContext from '../../components/context/ProjectActionContext';
import { Project } from '../../app/types/graphql-zeus';
import ProjectContext from '../../components/context/ProjectContext';
import { useRouter } from 'next/router';

const Projects: NextPage<{ user: Request['user']; project: Project }> = ({
                                                                           user,
                                                                           project,
                                                                         }) => {

  const router = useRouter();
  return (
    <ApolloProvider client={client}>
      <ProfileContext.Provider value={user}>
        <LayoutApp>
          <ProjectActionContext.Provider
            value={{
              onUpdate: () => {
                router.push(`/projects/${project.id}`);
              },
              onDelete: () => {
                router.push('/projects');
              },
            }}
          >
            <ProjectContext.Provider value={project}>
              <ProjectView />
            </ProjectContext.Provider>
          </ProjectActionContext.Provider>
        </LayoutApp>
      </ProfileContext.Provider>
    </ApolloProvider>
  );
};

export async function getServerSideProps({ req, query }) {
  const data = await typedQuery(
    {
      projectById: [
        { id: query.id },
        {
          alias: true, id: true, tasks: {
            id: true,
            complete: true,
            subject: true,
            updated_at: true,
            complete_date: true,
            created_at: true,
          },
        },
      ],
    },
    req,
  );
  return {
    props: { user: (req as Request).user, project: data.projectById },
  };
}

export default Projects;
