import { Inject, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { CurrentUser } from '../auth/graphql/gql-auth.decorator';
import { GqlAuthGuard } from '../auth/graphql/gql-auth.guard';
import { User } from '../users/user.entity';
import { ProjectsService } from './projects.service';
import { Project } from './project.entity';
import { Like } from 'typeorm';

@Resolver((_of) => Project)
export class ProjectsResolver {
  constructor(
    @Inject(ProjectsService) private projectsService: ProjectsService,
  ) {}

  @Query((_returns) => [Project])
  @UseGuards(GqlAuthGuard)
  projects(@CurrentUser() user: User) {
    return this.projectsService.findAll({ where: { user: user } });
  }

  @Query((_returns) => Project)
  @UseGuards(GqlAuthGuard)
  projectById(
    @CurrentUser() user: User,
    @Args({ name: 'id', type: () => Number }) id: number,
  ) {
    return this.projectsService.findOne({ where: { user: user, id } });
  }

  @Query((_returns) => [Project])
  @UseGuards(GqlAuthGuard)
  projectByAlias(
    @CurrentUser() user: User,
    @Args({ name: 'alias', type: () => String }) alias: string,
  ) {
    return this.projectsService.findAll({
      where: { user: user, alias: Like(`${alias}%`) },
    });
  }

  @Mutation((_returns) => Project)
  @UseGuards(GqlAuthGuard)
  createProject(
    @CurrentUser() user: User,
    @Args({ name: 'alias', type: () => String }) alias: string,
  ) {
    return this.projectsService.createProject({
      user: user,
      alias: alias,
    });
  }

  @Mutation((_returns) => Project)
  @UseGuards(GqlAuthGuard)
  updateAlias(
    @Args({ name: 'id', type: () => Number }) id: number,
    @Args({ name: 'alias', type: () => String }) alias: string,
  ) {
    return this.projectsService.update(id,{
      alias: alias,
    });
  }
  @Mutation((_returns) => Project)
  @UseGuards(GqlAuthGuard)
  removeProject(
    @Args({ name: 'id', type: () => Number }) id: number
  ) {
    return this.projectsService.delete(id);
  }
}
