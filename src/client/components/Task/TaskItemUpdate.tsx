import React from 'react';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import TaskUpdate from './Actions/TaskUpdate';
import IconButton from '@material-ui/core/IconButton';
import Close from '@material-ui/icons/Close';
import ListItem from '@material-ui/core/ListItem';

export default function TaskItemUpdate({ toggleOpen }) {
  return (
    <ListItem ContainerComponent={'div'}>
      <TaskUpdate handleClose={toggleOpen} />
      <ListItemSecondaryAction>
        <IconButton onClick={() => toggleOpen()}>
          <Close />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
}
