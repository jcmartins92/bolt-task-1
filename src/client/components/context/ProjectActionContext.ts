import { createContext } from 'react';

export interface ProjectAction {
  onUpdate?: () => void;
  onCreate?: () => void;
  onDelete?: () => void;
}

export default createContext<ProjectAction>({});
