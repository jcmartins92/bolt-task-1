import React, { useContext } from 'react';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import { pick } from 'lodash';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from 'react-hook-form';
import { Column, Item, Row } from '@mui-treasury/components/flex';
import ProjectDelete from './ProjectDelete';
import ProjectContext from '../context/ProjectContext';
import ProjectActionContext from '../context/ProjectActionContext';
import { responseToastError } from '../Login/helpers';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function saveProject(data, projectId?) {
  if (projectId) {
    return axios.put(`/api/projects/${projectId}`, data);
  }
  return axios.post('/api/projects', data);
}

export default function ProjectForm({ handleClose }) {
  const classes = useStyles();

  const project = useContext(ProjectContext);
  const { onCreate, onUpdate } = useContext(ProjectActionContext);
  const projectId = project ? project.id : undefined;
  const { register, handleSubmit } = useForm({
    defaultValues: pick(project, ['alias']),
  });
  const onSubmit = (data) => {
    saveProject(data, projectId)
      .then(function (response) {
        if (response.status >= 200) {
          if (projectId) {
            onUpdate();
          } else {
            onCreate();
          }
          handleClose();
        }
      })
      .catch(function (error) {
        responseToastError(error);
      });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.form} noValidate>
      <Column>
        <TextField
          variant={'outlined'}
          margin={'normal'}
          required
          fullWidth
          label={'Alias'}
          id={'alias'}
          autoFocus={true}
          inputProps={register('alias')}
        />
      </Column>
      <Column>
        <Row>
          <Item position={'middle'}>{projectId && <ProjectDelete />}</Item>
          <Item position={'right'}>
            <Row gap={1}>
              <Item position={'middle'}>
                <Button onClick={handleClose}>Cancel</Button>
              </Item>
              <Item>
                <Button
                  type={'submit'}
                  fullWidth
                  variant={'contained'}
                  color={'primary'}
                  className={classes.submit}
                >
                  {projectId ? 'Update' : 'Creates'}
                </Button>
              </Item>
            </Row>
          </Item>
        </Row>
      </Column>
    </form>
  );
}
