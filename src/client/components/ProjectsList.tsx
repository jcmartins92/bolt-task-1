import React, { Fragment, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import ProjectCard from './Project/ProjectCard';
import { useTypedQuery } from '../app/apollo-client';
import Typography from '@material-ui/core/Typography';
import AppSearchBar from './Structure/AppSearchBar';
import { Column } from '@mui-treasury/components/flex';
import Divider from '@material-ui/core/Divider';
import ProjectContext from './context/ProjectContext';
import ProjectActionContext from './context/ProjectActionContext';
import ProjectActionButton from './Project/ProjectActionButton';

const useStyles = makeStyles((theme) => ({
  paper: {
    maxWidth: 936,
    margin: 'auto',
    overflow: 'hidden',
  },
  searchBar: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
  },
  searchInput: {
    fontSize: theme.typography.fontSize,
  },
  block: {
    display: 'block',
  },
  addUser: {
    marginRight: theme.spacing(1),
  },
  contentWrapper: {
    margin: '40px 16px',
  },
}));
export default function ProjectsList({}) {
  const classes = useStyles();
  const [search, setSearch] = useState('');
  const { loading, error, data, refetch } = useTypedQuery({
    projectByAlias: [
      { alias: search },
      {
        alias: true,
        id: true,
        tasks: { complete: true, id: true },
        created_at: true,
        updated_at: true,
      },
    ],
  });

  const handleSearch = (value) => {
    setSearch(value);
  };
  const hasData = Boolean(data && data.projectByAlias.length);
  return (
    <ProjectActionContext.Provider
      value={{
        onCreate: refetch,
        onUpdate: refetch,
        onDelete: refetch,
      }}
    >
      <Paper className={classes.paper}>
        <AppSearchBar
          placeholder={'Search by project alias'}
          onSearch={handleSearch}
          search={search}
          extraAction={<ProjectActionButton label={'New Project'} />}
          loading={loading}
          onRefresh={refetch}
        />
        <div className={classes.contentWrapper}>
          {error && <Typography>{JSON.stringify(error)}</Typography>}
          {!hasData && <Typography>No Project Found</Typography>}
          {hasData &&
            data.projectByAlias.map((project) => (
              <Fragment key={project.id}>
                <Column m={{ xs: 1, sm: 2, md: 2 }}>
                  <ProjectContext.Provider value={project}>
                    <ProjectCard />
                  </ProjectContext.Provider>
                </Column>
                <Divider />
              </Fragment>
            ))}
        </div>
      </Paper>
    </ProjectActionContext.Provider>
  );
}
