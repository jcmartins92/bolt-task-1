import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import { responseToastError } from './helpers';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
export default function FormSingIn({}) {
  const classes = useStyles();
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({});
  const onSubmit = (data) => {
    axios
      .post('/auth/signup', data)
      .then((response) => {
        toast.success(`Welcome to BOLT......TASK ⚡`);
        if (response.status >= 200) {
          router.push('/projects');
        }
      })
      .catch((error) => {
        responseToastError(error);
      });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.form} noValidate>
      <TextField
        variant={'outlined'}
        margin={'normal'}
        required
        fullWidth
        id={'name'}
        label={'Name'}
        autoComplete={'name'}
        {...register('name')}
        autoFocus
      />
      <TextField
        variant={'outlined'}
        margin={'normal'}
        required
        fullWidth
        id={'username'}
        label={'Username'}
        autoComplete={'username'}
        {...register('username')}
      />
      <TextField
        variant={'outlined'}
        margin={'normal'}
        required
        fullWidth
        {...register('password')}
        label={'Password'}
        type={'password'}
        id={'password'}
        autoComplete={'current-password'}
      />
      <Button
        type={'submit'}
        fullWidth
        variant={'contained'}
        color={'primary'}
        className={classes.submit}
      >
        Login in
      </Button>
      <div style={{ color: 'red' }}>
        <pre>
          {Object.keys(errors).length > 0 && (
            <label>Errors: {JSON.stringify(errors, null, 2)}</label>
          )}
        </pre>
      </div>
      <Grid container>
        <Grid item>
          <Link href={'/login'} variant={'body2'}>
            You have an account? Sign In
          </Link>
        </Grid>
      </Grid>
    </form>
  );
}
