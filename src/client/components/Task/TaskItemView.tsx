import React, { useContext } from 'react';
import { Item, Row } from '@mui-treasury/components/flex';
import TaskContext from '../context/TaskContext';
import ToggleTask from './Actions/ToggleTask';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import { Info, InfoCaption, InfoTitle } from '@mui-treasury/components/info';
import TaskDelete from './Actions/TaskDelete';
import Done from '@material-ui/icons/Done';
import Chip from '@material-ui/core/Chip';
import ListItem from '@material-ui/core/ListItem';
import { formatDate, formatDateDistance } from '../helper.dates';

export default function TaskItemView({ toggleOpen }) {
  const { subject, complete, created_at, complete_date } = useContext(
    TaskContext,
  );
  return (
    <ListItem onClick={toggleOpen} ContainerComponent={'div'}>
      <ListItemText>
        <Info>
          <InfoTitle>{subject}</InfoTitle>
          <InfoCaption>
            <Row gap={1}>
              <Item>
                <Chip size={'small'} label={formatDate(created_at, 'Create')} />
              </Item>
              {complete_date && (
                <Item>
                  <Chip
                    icon={<Done />}
                    size={'small'}
                    label={formatDateDistance(complete_date, 'Complete')}
                  />
                </Item>
              )}
            </Row>
          </InfoCaption>
        </Info>
      </ListItemText>
      <ListItemSecondaryAction>
        <Row gap={2}>
          {!complete && (
            <Item>
              <TaskDelete />
            </Item>
          )}
          <Item>
            <ToggleTask />
          </Item>
        </Row>
      </ListItemSecondaryAction>
    </ListItem>
  );
}
