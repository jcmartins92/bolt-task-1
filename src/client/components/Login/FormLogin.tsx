import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import get from 'lodash/get';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function responseToastError(error) {
  if (get(error, 'response.data.message')) {
    toast.error(get(error, 'response.data.message'));
  } else {
    toast.error(`Is not supposed, maybe later will work.. ${error}`);
  }
}

export default function FormSingIn({}) {
  const classes = useStyles();
  const router = useRouter();
  const { register, handleSubmit } = useForm({});
  const onSubmit = (data) => {
    axios
      .post('/auth/login', data)
      .then(function (response) {
        if (response.status >= 200) {
          toast.success(`Hello again ⚡`);
          router.push('/projects');
        }
      })
      .catch(function (error) {
        responseToastError(error);
      });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.form} noValidate>
      <TextField
        variant={'outlined'}
        margin={'normal'}
        required
        fullWidth
        id={'username'}
        label={'Username'}
        autoComplete={'email'}
        autoFocus
        {...register('username')}
      />
      <TextField
        variant={'outlined'}
        margin={'normal'}
        required
        fullWidth
        label={'Password'}
        type={'password'}
        id={'password'}
        autoComplete={'current-password'}
        {...register('password')}
      />
      <Button
        type={'submit'}
        fullWidth
        variant={'contained'}
        color={'primary'}
        className={classes.submit}
      >
        Login in
      </Button>
      <Grid container>
        <Grid item>
          <Link href={'/register'} variant={'body2'}>
            {"Don't have an account? Sign Up"}
          </Link>
        </Grid>
      </Grid>
    </form>
  );
}
