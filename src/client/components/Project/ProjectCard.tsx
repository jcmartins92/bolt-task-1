import React, { useContext } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import { Column, Item, Row } from '@mui-treasury/components/flex';
import ProjectHeader from '../Structure/ProjectHeader';
import ProjectContext from '../context/ProjectContext';
import { useRouter } from 'next/router';
import { formatDate, formatDateDistance } from '../helper.dates';

const useStyles = makeStyles(() => ({
  card: {
    width: '100%',
    borderRadius: 16,
    boxShadow: '0 8px 16px 0 #BDC9D7',
    overflow: 'hidden',
  },
  link: {
    color: '#2281bb',
    cursor: 'pointer',
    padding: '0 0.25rem',
    fontSize: '0.875rem',
  },
}));

export default function ProjectCard() {
  const styles = useStyles();
  const { id, created_at, updated_at } = useContext(ProjectContext);
  const router = useRouter();
  return (
    <Column p={0} gap={0} className={styles.card}>
      <ProjectHeader
        extraActions={
          <>
            <Link
              className={styles.link}
              onClick={() => {
                router.push(`/projects/${id}`);
              }}
            >
              Detail •{' '}
            </Link>
          </>
        }
      />
      <Row p={2} justifyContent={'space-between'} alignItems={'baseline'}>
        <Item>{formatDate(created_at)}</Item>
        <Item>{formatDateDistance(updated_at)}</Item>
      </Row>
    </Column>
  );
}
