export class User {
  id: number;
  username: string;
  password: string;
  name: string;
  created_at: Date;
  updated_at: Date;
}
