import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from '../users/user.entity';
import { Project } from '../projects/project.entity';

@ObjectType()
@Entity()
export class Task {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ nullable: false })
  subject: string;

  @Field()
  @Column({ nullable: false, default: false })
  complete: boolean;

  @Field((_type) => User)
  @ManyToOne((_type) => User, (user) => user.tasks, { nullable: false })
  user: User;

  @Field((_type) => Project)
  @ManyToOne((_type) => Project, (project) => project.tasks, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  project: Project;

  @Field({ nullable: true })
  @Column({ nullable: true })
  complete_date?: Date;

  @Field()
  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Field()
  @Column()
  @UpdateDateColumn()
  updated_at: Date;
}
