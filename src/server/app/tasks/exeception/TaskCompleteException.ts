import { HttpStatus } from '@nestjs/common';

export class TaskCompleteException extends Error {
  constructor() {
    super('Task is complete');
  }
}
