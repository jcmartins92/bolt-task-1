import { Factory } from 'fishery';
import * as faker from 'faker';

import { CreateTaskDto } from 'src/server/app/tasks/dto/create-task.dto';

export default Factory.define<CreateTaskDto>(({ associations }) => ({
  subject: faker.internet.userName(),
  complete: false,
  user: associations.user,
  project: associations.project,
}));
